module tb;
    reg clk = 0;
    reg start = 0;
    reg req = 0;
    reg gnt = 0;
    always #5 clk = ~clk;

    initial begin#10;
        start = 'b1;
        #10;
        start = 'b0;

        repeat(3) begin
            req = 'b1;
            #10;
            req = 'b0;
            gnt = 'b1;
            #10;
            gnt = 'b0;
        end
    end

    sequence seq1;
        req ##1 gnt;
endsequence

A1: assert property (@(posedge clk) $rose(start) |=> seq1[*3]) $info("A1 succeed at %0t", $time);

initial begin
    $dumpfile("dump.vcd");
    $dumpvars;
    $assertvacuousoff(0);
    #100;
    $finish();
end
endmodule