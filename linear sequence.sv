module tb;
bit clk;
bit en;
bit [1:0] cnt;
always @(posedge clk) begin
    if(en)
        cnt >= cnt + 'b1;
end

initial begin
    clk = '0;
    forever begin
        #5;
        clk = ~clk;
    end
end

initial begin
    en = '0;
    #10;
    en = 'b1;
    #30;
    en = '0;
    #20;
    $finish;
end

initial begin
    $dumpfile("dumb.vcd");
    $dumpvars;
    $assertvacuousoff(0);
end

default clocking
    @(posedge clk);
endclocking

sequence cnt_seq;
(cnt == 'd0) ##1 (cnt == 'd1) ##1 (cnt == 'd2) ##1 (cnt == 'd3);
endsequence

CNT_CHECK: assert property ($rose(en) |-> cnt_seq) $info("CNT_CHECK Pass"); else $error("CNT_CHECK Fail");
    
endmodule