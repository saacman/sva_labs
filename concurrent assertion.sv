// Como añadir aserciones a las interfaces
interface mem_if(input bit clk);
  bit [9:0] addr;
  bit wr_en;
endinterface

module tb;
`define period 10ns

  bit clk;
  mem_if mem_intf(clk);

  initial
  begin
    clk = '0;
    forever
    begin
      #(`period/2);
      clk = ~clk;
    end
  end

  initial
  begin
    repeat(5)
    begin
      mem_intf.addr = '1;
      mem_intf.wr_en = '0;
      #(`period);
      mem_intf.wr_en = '1;
      #(`period);
      mem_intf.addr = '0;
      #(`period);
      mem_intf.wr_en = '0;
      #(`period);
    end
    $finish;
  end

  initial
  begin
    $dumpfile("dumb.vcd");
    $dumpvars;
    $assertvacuousoff(0);
  end

  default clocking
            @(posedge clk);
          endclocking

        A1:
            assert property (@(negedge clk) !(mem_intf.wr_en && mem_intf == 0));
          A2:
                assert property (!(mem_intf.wr_en && mem_intf.addr == 0));
  endmodule
