module tb;
  reg clk = 0;
  reg req = 0;
  reg ack = 0;
  reg rst = 0;

  always #5 clk = ~clk;

  initial
  begin
    req = 'b1;
    ack = 'b1;
    #10;
    ack = 'b0;
    #10;
    req = 'b1;
    ack = 'b0;
    #10;
  end

  always @(posedge clk)
  begin
A1:
      assert #0 (req == ack);
    if(rst)
      disable A1;
  end

A2:
    assert property ( @(posedge clk) disable iff (rst) req |-> ack);

      initial
      begin
        $dumpfile("dumb.vcd");
        $dumpvars;
        $assertvacuousoff(0);
      end
      initial
      begin
        $assertoff(0);
        #50;
        $finish();
      end
    endmodule
