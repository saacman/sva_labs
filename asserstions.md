```verilog
module tb;
reg a=1, clk = 0;

always #5 clk = ~clk;
always #5 a = ~a;

always @(posedge clk)
begin
$info("Value of a :%b and $sampled (a) :%b ", a, $sampled(a));
end
//assert property (@(posedge clk) a $sampled(a)) $info( "Suc at %0t with
// a:%0b",$time, $sampled(a)), // $sampled(a) returns value in preponed
// region, and simple a returns value
//if a or $sampled(a) are used within a concurrent assertions, the result
// is the same, thus, simple a must be used
initial begin
$dumpfile("dump .vcd");
$dumpvars;
//$assertvacuousoff(0);
#120;
$finish()
end
endmodule
```

```verilog
module tb;
reg a, clk=0;
reg c;
reg [3:0] b;

always #5 clk = ~clk;
//always #2 en = ~en;

initial begin
#10;
a = 0;
#20;
a =1;
#20;
a = 0;
end

initial begin
b = 4'b0100;
#10;
b = 4'b0101;
#16
b = 4'b0100;
#10;
b = 4'b0101;
#10;
b = 4'b0100;
#10;
b = 4'b0100;
#10;
b = 4'b0000;
end
initial begin
    $dumpfile( "dump.vcd");

$dumpvars;
//$assertvacuousoff(0);
$finish();
end
always@(posedge clk)
begin
$info("Value of b : %b and $rose(b): %b", $sampled(b), $rose($sampled(b)));
end
assert property (@(posedge clk) $rose(a));
assign c = $rose(a, @(posedge clk)),

endmodule
```

```verilog
module tb;
reg a = 1, clk = 0;
reg en = 0;
reg [3:0] b = 0;
integer i = 0;

always #5 clk = ~ clk;


initial begin
en = 0;
#20;
en = 1;
#100;
en = 0;
end

initial begin
for(i = 0; i< 15; i++) begin
b <= b+1;
@(posedge clk);
end
end

initial begin
$dumpfile("dump.vcd");
$dumpvars;
$assertvacuousoff(0);
#200;
$finish();
end

always @(posedge clk)
begin
$info("Value of $past(a): %0d at time %0t", $past(b,1,en), $time);
end
asset property (@(posedge clk) $fell(en) |-> ##3 ($past(b,1,en)==4'hb)) $info("Suc at%0t", $time);
endmodule
```
