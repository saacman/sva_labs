module tb;
	typedef enum logic [1:0] {SIMPLE_MODE, BURST_MODE, PAGE_MODE} modes_t;

    reg clk = 0;
    reg req = 0;
    reg busy = 0;
    reg rstn = 0;
	
	modes_t mode;
	
	always #5 clk = ~clk;
	initial begin
		mode = SIMPLE_MODE;
		rstn = 'b1;
		busy = 'b0;
		req = 'b1;
		#10;
		req = 'b0;
		#10;
		req = 'b1;
		#10;
		#10;
		req = 'b0;
		#10;
		$finish;
	end
	
	function bit disable_fnc();
		return (!rstn || busy);
	endfunction
	
	MODE_CHECK: assert property (@(posedge clk) disable iff(!rstn || busy) (req && mode == SIMPLE_MODE) |-> !req) else $error("Two consecutive req detected in SIMPLE_MODE at time %0t", $time);
	MODE_CHECK_FNC: assert property (@(posedge clk) disable iff(disable_fnc()) (req && mode == SIMPLE_MODE) |-> !req) else $error("Two consecutive req detected in SIMPLE_MODE at time %0t", $time);
	
initial begin
	$dumpfile("dumb.vcd");
    $dumpvars;
    $assertvacuousoff(0);
	end

endmodule
